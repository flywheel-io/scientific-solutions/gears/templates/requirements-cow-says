"""Module to test parser.py"""
from unittest.mock import MagicMock

from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_cow_says.parser import parse_config


def test_parse_config():
    gear_context = MagicMock(spec=GearToolkitContext)

    parse_config(gear_context)

    assert gear_context.get_input_path.call_count == 1
    assert gear_context.get_input.call_count == 0
