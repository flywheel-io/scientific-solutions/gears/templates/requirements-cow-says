"""Module to test main.py"""
import pytest

from fw_gear_cow_says.main import run


@pytest.mark.parametrize("animal", ["cow", "dragon"])
def test_run(mocker, animal):
    cowsay = mocker.patch("fw_gear_cow_says.main.cowsay")
    run(animal, "Hello")

    getattr(cowsay, animal).assert_called_once_with("Hello")
