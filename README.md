This is a bare bone gear making animals speak. 

# Cowsay Gear

Implementation of cowsay.

```
  ______________
< Flywheel rocks >
  ==============
            \
             \
               ^__^                             
               (oo)\_______                   
               (__)\       )\/\             
                   ||----w |           
                   ||     ||  
```                   

## Usage

### Inputs

* __text-file__: A text file containing text to say.

### Configuration
* __debug__ (boolean, default False): Include debug statements in output.
* __animal__ (string, default 'cow'): Animal to say text.
