FROM python:3.8-slim as base

ENV FLYWHEEL="/flywheel/v0"
WORKDIR ${FLYWHEEL}

# Dev install. git for pip editable install.
RUN apt-get update && apt-get install -y git

COPY requirements.txt ./
RUN pip install -r requirements.txt

# These layers are the most the most likely to change during dev and
# are kept at the bottom for build time optimization.
COPY run.py $FLYWHEEL/
COPY fw_gear_cow_says $FLYWHEEL/fw_gear_cow_says

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
WORKDIR $FLYWHEEL
ENTRYPOINT ["python","/flywheel/v0/run.py"]
